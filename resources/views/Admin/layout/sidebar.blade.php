<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#"><i class="fas fa-book-medical"></i> Klinik Mediatest</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">MT</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown text-dark"><i
                        class="fas fa-book-medical"></i><span>Mediatest
                        Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li class="active"><a class="nav-link" href="{{ route('dashboard') }}">Admin Dashboard</a></li>
                </ul>
            </li>
            <li class="menu-header">Pasien</li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown text-dark"><i
                        class="fas fa-user-friends"></i><span>Pasien</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('daftarpasien') }}">Pendaftaran Pasien</a>
                    </li>
                    <li><a class="nav-link" href="{{ route('data_pasien') }}">Data Pasien</a></li>
                </ul>
            </li>

            <li class="menu-header"> Pemeriksaan </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown text-dark" data-toggle="dropdown"><i
                        class="fas fa-columns"></i>
                    <span>Data Pemeriksaan</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('tampilPeriksaPasien') }}">Periksa Pasien</a></li>
                    <li><a class="nav-link" href="{{ route('tampilDataPemeriksaan') }}">Data Pemeriksaan</a></li>
                    <li><a class="nav-link" href="{{ route('tampilHasilPeriksa') }}">Data Hasil Pemeriksaan</a>
                    </li>
                    <!-- <li><a class="nav-link" href="{{ route('tampilHasil') }}">Hasil Periksa</a></li> -->
                    <!-- <li><a class="nav-link" href="#">Cetak Hasil Pemeriksaan</a></li> -->
                </ul>
            </li>
            <li class="menu-header">Mediatest Menu</li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown text-dark"><i
                        class="fas fa-sign-out-alt"></i><span>Menu</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('logout') }}">Keluar</a></li>
                </ul>
            </li>

            <!-- <ul class="sidebar-menu">
                <a href="" class="nav-link has-dropdown"></i><span>Logout</span></a>
            </ul> -->
    </aside>
</div>
