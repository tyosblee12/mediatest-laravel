@extends('Admin.layout.master');
@section('content');

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Kategori Penyakit</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Kategori Penyakit</a></div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Kategori Penyakit</h2>
            <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Input Kategori Penyakit</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6">
                                    <!-- KOLOM 1 -->
                                    <form action="{{route('simpan_kategori_penyakit')}}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>Id Kategori Penyakit</label>
                                            @foreach ($Id_Kategori as $row)
                                            @endforeach
                                            <input type="text" class="form-control" value="{{$row->Id_Kategori + 1}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Kategori</label>                    
                                            <select class="form-control selectric">
                                            <option>Hematologi</option>
                                            <option>Kimia Klinik</option>
                                            <option>Paket Ibu hamil</option>
                                            <option>Immuno-Serologi</option>
                                            <option>Urinalisis</option>
                                            <option>Faeces</option>
                                            <option>Mikrobiologi</option>
                                            <option>Rontgen</option>
                                            </select>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection