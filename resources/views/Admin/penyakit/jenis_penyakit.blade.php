@extends('Admin.layout.master');
@section('content');

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Jenis Penyakit</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Jenis Penyakit</a></div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Jenis Penyakit</h2>
            <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Input Jenis Penyakit</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6">
                                    <!-- KOLOM 1 -->
                                    <form action="{{route('simpan_jenis_penyakit')}}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>Id Jenis Penyakit</label>
                                            @foreach ($Id_Penyakit as $row)
                                            @endforeach
                                            <input type="text" class="form-control" value="{{$row->Id_Penyakit + 1}}" disabled>
                                        </div>
                                      
                                        <div class="form-group">
                                            <label>Nama Penyakit</label>
                                            <input type="text" name="Nama_Penyakit" id="Nama_Penyakit" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input type="text" name="Harga" id="Harga" class="form-control">
                                        </div>
                                        
                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection