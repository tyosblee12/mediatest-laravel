@extends('Admin.layout.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Selamat Datang, {{ session('name') }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item">Data</div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1 shadow-lg">
                        <div class="card-icon bg-primary">
                            <i class="far fa-user"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-body pt-3">
                                <h5>{{ $pasien }} Orang</h5>
                            </div>
                            Jumlah Seluruh Pasien
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 ">
                    <div class="card card-statistic-1 shadow-lg">
                        <div class="card-icon bg-danger">
                            <i class="far fa-newspaper"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-body pt-3">
                                <h5>{{ $pasien }} Orang</h5>
                            </div>
                            Jumlah Pasien Hari Ini
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 ">
                    <div class="card card-statistic-1 shadow-lg">
                        <div class="card-icon bg-warning">
                            <i class="far fa-file"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-body pt-3">
                                <h5>Data Pemeriksaan</h5>
                            </div>
                            Pengelolaan Data
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead class="bg-primary">
                                            <tr>
                                                <th class="text-white">No</th>
                                                <th class="text-white">Nama Pasien</th>
                                                <th class="text-white">Umur</th>
                                                <th class="text-white">Jenis Kelamin</th>
                                                <th class="text-white">Alamat Pasien</th>
                                                <th class="text-white">Rujukan</th>
                                                <th class="text-white">Alamat Rujukan</th>
                                                <th class="text-white">No Telepon Pasien</th>
                                                <th class="text-white">Tanggal Daftar</th>
                                                <th scope="row">#</th>
                                            </tr>
                                            <thead>
                                            <tbody>
                                                @foreach ($datapasien as $row)
                                                    <tr>
                                                        <td>{{ $row->No_Registrasi }}</td>
                                                        <td>{{ $row->Nama_Pasien }}</td>
                                                        <td>{{ $row->Umur }}</td>
                                                        <td>{{ $row->Jenis_Kelamin }}</td>
                                                        <td>{{ $row->Alamat_Pasien }}</td>
                                                        <td>{{ $row->Rujukan }}</td>
                                                        <td>{{ $row->Alamat_Rujukan }}</td>
                                                        <td>{{ $row->No_Telepon_Pasien }}</td>
                                                        <td>{{ $row->Tanggal_Daftar }}</td>
                                                        <td scope="row">
                                                            <div class="btn-group mb-3" role="group"
                                                                aria-label="Basic example">
                                                                <a href="{{ route('edit_data', $row->No_Registrasi) }}"
                                                                    type="button" class="btn btn-sm btn-primary"><i
                                                                        class="fas fa-edit"></i></a>
                                                                <a href="{{ route('periksa_data', $row->No_Registrasi) }}"
                                                                    type="button" class="btn btn-sm btn-warning"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <a href="{{ route('delete_data', $row->No_Registrasi) }}"
                                                                    type="button" class="btn btn-sm btn-danger"><i
                                                                        class="fas fa-trash"></i></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
@endsection
