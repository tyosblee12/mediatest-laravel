@extends('Admin.layout.master')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Hasil Pemeriksaan </h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Pemeriksaan</a></div>
                <div class="breadcrumb-item">Hasil Pemeriksaan </div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Tabel Hasil Pemeriksaan </h2>
            <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <!-- KOLOM 1 -->
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID Hasil Periksa</th>
                                                <th scope="col">No Registrasi</th>
                                                <th scope="col">Nama Pasien</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                            <thead>
                                            <tbody>
                                                @foreach ($datapemeriksaan as $row)
                                                <td name="Id_Hasil_Periksa">{{$row->Id_Jenis}}</td>
                                                <td>{{$row->Tanggal_Periksa}}</td>
                                                <td name="No_Registrasi">{{$row->No_Registrasi}}</td>
                                                <td>{{$row->havePasien[0]->Nama_Pasien}}</td>
                                                <td>{{$row->id_jenis}}</td>
                                                <td>{{$row->haveJenis[0]->nama_jenis}}</td>
                                                <td>{{$row->haveJenis[0]->Harga}}</td>

                                                <td>
                                                    <div class="btn-group mb-3" role="group" aria-label="Basic example">
                                                        <a href="{{route('editDataPemeriksaan', $row->Id_Periksa)}}"
                                                            type="button" class="btn btn-primary"><i
                                                                class="fas fa-edit"></i> Ubah</a>
                                                        <!-- FORM -->
                                                        <form
                                                            action="{{route('hasilDataPemeriksaan', $row->Id_Periksa)}}"
                                                            method="post">
                                                            @csrf
                                                            <button type="submit" class="btn btn-warning"><i
                                                                    class="fas fa-pencil-alt"></i> Hasil</button>
                                                            <input type="hidden" value="{{$row->Id_Periksa}}"
                                                                name="Id_Periksa"></input>
                                                            <input type="hidden" value="{{$row->No_Registrasi}}"
                                                                name="No_Registrasi"></input>
                                                        </form>

                                                        <a href="{{route('deleteDataPemeriksaan', $row->Id_Periksa)}}"
                                                            type="button" class="btn btn-danger"><i
                                                                class="fas fa-trash"></i> Hapus</a>
                                                    </div>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>


@endsection