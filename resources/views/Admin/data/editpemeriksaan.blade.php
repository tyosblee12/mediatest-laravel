@extends ('Admin.layout.master')
@section ('content')


<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Edit Data Pemeriksaan</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Data</a></div>
                <div class="breadcrumb-item">Pemeriksaan </div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Pemeriksaan</h2>
            <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Tabel</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6">
                                    <!-- KOLOM 1 -->
                                    <form action="{{ route('updateDataPemeriksaan', $periksa->Id_Periksa) }}"
                                        method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>ID Periksa</label>
                                            <input type="text" name="Id_Periksa" id="Id_Periksa"
                                                value="{{ $periksa->Id_Periksa }}" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>No Registrasi</label>
                                            <input type="text" name="No_Registrasi" id="No_Registrasi"
                                                value="{{ $periksa->No_Registrasi }}" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pasien</label>
                                            <input type="text" name="Nama_Pasien" id="Nama_Pasien"
                                                value="{{$periksa->havePasien[0]->Nama_Pasien}}" class="form-control"
                                                disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Periksa</label>
                                            <input type="date" name="Tanggal_Periksa" id="Tanggal_Periksa"
                                                value="{{ $periksa->Tanggal_Periksa }}" class="form-control">
                                        </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <!-- KOLOM 2 -->
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="id_jenis">Jenis Pemeriksaan</label>
                                            <select class="form-control select2" id="id_jenis" name="id_jenis">
                                                <option class="font-weight-bold" value="{{$periksa->haveJenis[0]->name_jenis}}">{{$periksa->haveJenis[0]->nama_jenis}} - {{$periksa->haveJenis[0]->Harga}} </option>
                                                @foreach ($jenis as $poli)
                                                <option class="font-weight-bold" value="{{ $poli->id_jenis }}">
                                                    {{ $poli->nama_jenis }} - {{ $poli->Harga }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right float-right">
                                    <button class="btn btn-primary mr-1" type="submit">Update</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



@endsection