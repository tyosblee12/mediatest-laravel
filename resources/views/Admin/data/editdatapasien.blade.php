@extends('Admin.layout.master');
@section('content');

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Edit Data Pasien</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Pasien</a></div>
                <div class="breadcrumb-item">Edit Data Pasien </div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Pasien</h2>
            <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Tabel</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6">
                                    <!-- KOLOM 1 -->
                                    <form action="{{ route('update_data', $pasien->No_Registrasi) }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>No registrasi</label>
                                            <input type="text" name="No_Registrasi" id="No_Registrasi"
                                                value="{{ $pasien->No_Registrasi }}" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pasien</label>
                                            <input type="text" name="Nama_Pasien" id="Nama_Pasien"
                                                value="{{ $pasien->Nama_Pasien }}" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Umur</label>
                                            <input type="text" name="Umur" id="Umur" value="{{ $pasien->Umur }}"
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="d-block">Jenis Kelamin</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="Jenis_Kelamin" value="Laki-laki">Laki-laki<br>
                                                <input class="form-check-input" type="radio" name="Jenis_Kelamin" value="Perempuan">Perempuan<br>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Pasien</label>
                                            <input type="text" name="Alamat_Pasien" id="Alamat_Pasien"
                                                value="{{ $pasien->Alamat_Pasien }}" class="form-control">
                                        </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <!-- KOLOM 2 -->
                                    <div class="form-group">
                                        <label>No Telepon Pasien</label>
                                        <input type="text" name="No_Telepon_Pasien" id="No_Telepon_Pasien"
                                            value="{{ $pasien->No_Telepon_Pasien }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Rujukan</label>
                                        <input type="text" name="Rujukan" id="Rujukan" value="{{ $pasien->Rujukan }}"
                                            class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat Rujukan</label>
                                        <input type="text" name="Alamat_Rujukan" id="Alamat_Rujukan"
                                            value="{{ $pasien->Alamat_Rujukan }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Daftar</label>
                                        <input type="date" name="Tanggal_Daftar" id="Tanggal_Daftar"
                                            value="{{ $pasien->Tanggal_Daftar }}" class="form-control">
                                    </div>
                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary mr-1" type="submit">Update</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection