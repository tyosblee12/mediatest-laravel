<!-- <div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Hasil Pemeriksaan </h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Pemeriksaan</a></div>
                <div class="breadcrumb-item">Hasil Pemeriksaan </div>
            </div>
        </div> -->
<style>
#cetak {
    padding: 10px;
    font-family : Calibri;
}

table {
    width: 100%;
}

table,
th,
td {
    border: 1px solid;
}

thead {
    background-color: #54BAB9;
}

tfoot {
    font-weight: bold;
}
</style>
<div class="row" id="cetak">
    <div class="section-body">
        <h2 class="section-title">Tabel Hasil Pemeriksaan Pasien</h2>
        <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="nama_pasien" class="text-bold text-primary"> ID PERIKSA </label>
                        <h1 id="nama_pasien"> {{$hasil->Id_Periksa}}</h1>
                    </div>
                    <div class="col-md-4">
                        <label for="nama_pasien" class="text-bold text-primary"> NAMA PASIEN </label>
                        <h1 id="nama_pasien"> {{$hasil->havePasien[0]->Nama_Pasien}}</h1>
                    </div>
                    <div class="col-md-4">
                        <label for="nama_pasien" class="text-bold text-primary"> NO REGISTRASI PASIEN </label>
                        <h1 id="nama_pasien"> {{$hasil->No_Registrasi}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="bg-primary">
                                    <tr class="text-light">
                                        <th scope="row" class="text-light">ID Jenis</th>
                                        <th scope="row" class="text-light">Nama Pemeriksaan</th>
                                        <th scope="row" class="text-light">Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jenis as $je)
                                    <tr>
                                        <td>{{$je->id_jenis}}</td>
                                        <td>{{$je->haveJenis[0]->nama_jenis}}</td>
                                        <td>{{$je->haveJenis[0]->Harga}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan=2 class="text-center font-weight-bold">TOTAL</td>
                                        <td class="bg-grey text-center font-weight-bold">{{$total}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- <a href="#" class="btn-lg btn-primary"> Cetak </a> -->
                            <script>
                            window.print();
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>