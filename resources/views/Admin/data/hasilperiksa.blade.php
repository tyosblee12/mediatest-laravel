@extends('Admin.layout.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Selamat Datang, {{ session('name') }} </h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Periksa</a></div>
                    <div class="breadcrumb-item">Hasil</div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card  shadow-lg">
                            <div class="card-header bg-primary text-white">
                                Tabel Hasil Pemeriksaan
                            </div>
                            <div class="card-body">
                                <!-- KOLOM 1 -->
                                <div class="table table-responsive-sm">
                                    <table class="table table-hover table-bordered">
                                        <thead class="bg-primary">
                                            <tr>
                                                <th class="col-1 text-white">ID</th>
                                                <th class="col-1 text-white">Periksa</th>
                                                <th class="col-1 text-white">ID Pasien</th>
                                                <th class="col-2 text-white">Nama Pasien</th>
                                                <th class="col-2 text-white">Tanggal Periksa</th>
                                                <th class="col-1 text-white">#</th>
                                            </tr>
                                            <thead>
                                            <tbody>
                                                @foreach ($hasil as $p)
                                                    <tr>
                                                        <td>{{ $p->id_hasil }}</td>
                                                        <td>{{ $p->id_periksa }}</td>
                                                        <td>{{ $p->havePasien[0]->No_Registrasi }}</td>
                                                        <td>{{ $p->havePasien[0]->Nama_Pasien }}</td>
                                                        <td>{{ $p->tanggal_periksa }}</td>
                                                        <td class="align-content-around">
                                                            <div class="btn-group ml-3 ">
                                                                <a href="{{ route('tampilHasilDetail', $p->id_hasil) }}"
                                                                    class="btn btn-icon btn-warning" data-toggle="tooltip"
                                                                    data-placement="left"
                                                                    title="Lihat Hasil {{ $p->havePasien[0]->Nama_Pasien }}"><i
                                                                        class="far fa-eye"></i></a>
                                                                <a href="#" class="btn btn-icon btn-primary"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Lihat Hasil {{ $p->havePasien[0]->Nama_Pasien }}"><i
                                                                        class="far fa-edit"></i></a>
                                                                <a href="{{ route('hapusHasilPeriksa', $p->id_hasil) }}"
                                                                    class="btn btn-icon btn-danger" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    title="Hapus Hasil {{ $p->havePasien[0]->Nama_Pasien }}"><i
                                                                        class="fas fa-trash"></i></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
