@extends('Admin.layout.master');
@section('content');

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Pemeriksaan Pasien</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Pemeriksaan</a></div>
                <div class="breadcrumb-item">Data Pemeriksaan</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Tambah Data Pemeriksaan</h2>
            <p class="section-lead">Laboratorium Klinik MEDIATEST Soreang : Kepercayaan anda, Kebahagiaan Kami.</p>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <!-- KOLOM 1 -->
                                    <form action="{{route('simpan_data_periksa')}}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>Id Periksa</label>
                                            <input type="text" name="Id_Periksa" id="Id_Periksa" class="form-control"
                                                value="{{$id_periksa}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>No Registrasi</label>
                                            <input type="text" name="No_Registrasi" id="No_Registrasi"
                                                class="form-control" value="{{$pasien->No_Registrasi}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pasien</label>
                                            <input type="text" name="Nama_Pasien" id="Nama_Pasien" class="form-control"
                                                value="{{$pasien->Nama_Pasien}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="Tanggal_Periksa">Tanggal_Periksa</label>
                                            <input type="date" name="Tanggal_Periksa" id="Tanggal_Periksa"
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="id_jenis">Jenis Pemeriksaan</label>
                                            <select class="form-control select2" id="id_jenis" name="id_jenis">
                                                @foreach ($jenis as $poli)
                                                <option class="font-weight-bold" value="{{ $poli->id_jenis }}"> {{ $poli->nama_jenis }} - {{ $poli->Harga }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection