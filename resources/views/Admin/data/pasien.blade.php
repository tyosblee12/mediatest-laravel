@extends('Admin.layout.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Pasien </h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Pasien</a></div>
                    <div class="breadcrumb-item">Data Pasien </div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card shadow-lg">
                            <div class="card-header bg-primary text-white font-weight-bold">
                                <div class="col-md-10">
                                    Tabel Data Pasien
                                </div>
                                <div class="col-md-2 ">
                                    <button class="btn btn-secondary float-right shadow-none" href=""
                                        id="editCompany" data-toggle="modal" data-target='#practice_modal' data-id="">
                                        <i class="fas fa-plus"></i> &nbsp; Tambah</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- KOLOM 1 -->
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>No Registrasi</th>
                                                <th>Nama Pasien</th>
                                                <th>Umur</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Alamat Pasien</th>
                                                <th>Rujukan</th>
                                                <th>Alamat Rujukan</th>
                                                <th>No Telepon Pasien</th>
                                                <th>Tanggal Daftar</th>
                                                <th scope="row">Action</th>
                                            </tr>
                                            <thead>
                                            <tbody>
                                                @foreach ($datapasien as $row)
                                                    <tr>
                                                        <td>{{ $row->No_Registrasi }}</td>
                                                        <td>{{ $row->Nama_Pasien }}</td>
                                                        <td>{{ $row->Umur }}</td>
                                                        <td>{{ $row->Jenis_Kelamin }}</td>
                                                        <td>{{ $row->Alamat_Pasien }}</td>
                                                        <td>{{ $row->Rujukan }}</td>
                                                        <td>{{ $row->Alamat_Rujukan }}</td>
                                                        <td>{{ $row->No_Telepon_Pasien }}</td>
                                                        <td>{{ $row->Tanggal_Daftar }}</td>
                                                        <td scope="row">
                                                            <div class="btn-group mb-3" role="group"
                                                                aria-label="Basic example">
                                                                <a href="{{ route('edit_data', $row->No_Registrasi) }}"
                                                                    type="button" class="btn btn-sm btn-primary"><i
                                                                        class="fas fa-edit"></i></a>
                                                                {{-- <a href="{{ route('periksa_data', $row->No_Registrasi) }}"
                                                                    type="button" class="btn btn-sm btn-warning"><i
                                                                        class="fas fa-pencil-alt"></i></a> --}}
                                                                <a href="{{ route('delete_data', $row->No_Registrasi) }}"
                                                                    type="button" class="btn btn-sm btn-danger"><i
                                                                        class="fas fa-trash"></i></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="practice_modal">
        <div class="modal-dialog">
            <form action="{{ route('simpan_daftar_pasien') }}" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Data Pasien</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <!-- KOLOM 1 -->

                                <div class="form-group">
                                    <label>No registrasi</label>
                                    @foreach ($no_reg as $row)
                                    @endforeach
                                    <input type="text" class="form-control" value="{{ $row->No_Registrasi + 1 }}"
                                        disabled>
                                </div>

                                <div class="form-group">
                                    <label>Nama Pasien</label>
                                    <input type="text" name="Nama_Pasien" id="Nama_Pasien" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Umur</label>
                                    <input type="text" name="Umur" id="Umur" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="d-block">Jenis Kelamin</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Jenis_Kelamin"
                                            value="Laki-laki">Laki-laki<br>
                                        <input class="form-check-input" type="radio" name="Jenis_Kelamin"
                                            value="Perempuan">Perempuan<br>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Alamat Pasien</label>
                                    <input type="text" name="Alamat_Pasien" id="Alamat_Pasien" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>No Telepon Pasien</label>
                                    <input type="text" name="No_Telepon_Pasien" id="No_Telepon_Pasien"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Rujukan</label>
                                    <input type="text" name="Rujukan" id="Rujukan" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Alamat Rujukan</label>
                                    <input type="text" name="Alamat_Rujukan" id="Alamat_Rujukan" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Daftar</label>
                                    <input type="date" name="Tanggal_Daftar" id="Tanggal_Daftar"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
@endsection
