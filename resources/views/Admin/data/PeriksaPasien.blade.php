@extends('Admin.layout.master')
@section('content')
    <div class="main-content">
        {{-- @notifyCss --}}
        <section class="section">
            <div class="section-header">
                <h1>Selamat Datang, {{ session('name') }} </h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Pasien</a></div>
                    <div class="breadcrumb-item">Periksa Pasien</div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-6 col-lg-12">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-8">
                                <div class="card shadow-lg">
                                    <div class="card-header bg-primary text-white font-weight-bold">
                                        Pilih Jenis Pemeriksaan
                                    </div>
                                    <div class="card-body">
                                        <form action="{{ route('simpanJenisPeriksaPasien') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label>Nomor Pemeriksaan</label>
                                                @foreach ($id_periksa as $row)
                                                @endforeach
                                                @if ($baru == 1)
                                                    <input type="text" class="form-control text-bold" id="id_periksa"
                                                        name="id_periksa" value="{{ $row->id_periksa + 1 }}" readonly>
                                                @elseif($baru == 0)
                                                    <input type="text" class="form-control text-bold" id="id_periksa"
                                                        name="id_periksa" value="{{ $row->id_periksa }}" readonly>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Id Petugas</label>
                                                <input type="text" class="form-control" id="id_user" name="id_user"
                                                    value="{{ session('id_user') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="id_jenis">Jenis Pemeriksaan</label>
                                                <select class="form-control select2" id="id_jenis" name="id_jenis">
                                                    @foreach ($jenis as $poli)
                                                        <option class="font-weight-bold" value="{{ $poli->id_jenis }}">
                                                            {{ $poli->nama_jenis }} - {{ $poli->Harga }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group text-right">
                                                <button class="btn btn-primary mr-1" type="submit" data-toggle="tooltip"
                                                    data-placement="left" title="Tambah Jenis Periksa">Tambahkan</button>
                                            </div>
                                        </form>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Transaksi</th>
                                                        <th>Nama Jenis</th>
                                                        <th>Harga</th>
                                                        <th scope="row">#</th>
                                                    </tr>
                                                    <thead>
                                                    <tbody>
                                                        @foreach ($periksa as $p)
                                                            <tr>
                                                                <td>{{ $p->id_periksa }}</td>
                                                                <td>{{ $p->haveJenis[0]->nama_jenis }}</td>
                                                                <td>{{ $p->haveJenis[0]->Harga }}</td>
                                                                <td scope="row">
                                                                    <div class="btn-group m-3" role="group"
                                                                        aria-label="Basic example">
                                                                        <a href="{{ route('hapusPeriksaPasien', [$p->id_periksa, $p->id_jenis]) }}"
                                                                            type="button" class="btn btn-sm btn-danger"
                                                                            data-toggle="tooltip" data-placement="top"
                                                                            title="Hapus Jenis {{ $p->haveJenis[0]->nama_jenis }}"><i
                                                                                class="fas fa-trash"></i></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card shadow-lg">
                                    <div class="card-header bg-primary text-white font-weight-bold">
                                        Pilih Pasien
                                    </div>
                                    <div class="card-body">
                                        <!-- KOLOM 2 -->
                                        <form action="{{ route('simpanHasilPeriksa') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label>Nomor Pemeriksaan</label>
                                                @foreach ($id_periksa as $row)
                                                @endforeach
                                                @if ($baru == 1)
                                                    <input type="text" class="form-control" id="id_periksa"
                                                        name="id_periksa" value="{{ $row->id_periksa + 1 }}" readonly>
                                                @elseif($baru == 0)
                                                    <input type="text" class="form-control" id="id_periksa"
                                                        name="id_periksa" value="{{ $row->id_periksa }}" readonly>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Nomor Hasil</label>
                                                @foreach ($hasil as $h)
                                                @endforeach
                                                <input type="text" name="id_hasil" id="id_hasil"
                                                    value="{{ $h->id_hasil + 1 }}" class="form-control" readyonly>
                                            </div>
                                            <div class="form-group">
                                                {{-- <label>Nama Petugas</label> --}}
                                                <input type="hidden" name="id_user" id="id_user"
                                                    value="{{ session('id_user') }}" class="form-control" readyonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Tanggal Periksa</label>
                                                <input type="date" name="tanggal_periksa" id="tanggal_periksa"
                                                    class="form-control" value="@php echo date('Y-m-d') @endphp" readyonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="No_Registrasi">Pilih Pasien</label>
                                                <select class="form-control selectpicker" id="No_Registrasi"
                                                    name="No_Registrasi" data-live-search="true">
                                                    @foreach ($pasien as $p)
                                                        <option class="font-weight-bold"
                                                            value="{{ $p->No_Registrasi }}">
                                                            {{ $p->No_Registrasi }} - {{ $p->Nama_Pasien }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group text-right">
                                                <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
@endsection
