@extends('Admin.layout.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Hallo, {{ session('name') }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Pasien</a></div>
                    <div class="breadcrumb-item">Pendaftaran Pasien </div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card shadow-lg">
                            <div class="card-header bg-primary text-white font-weight-bold">
                                Tambah Data Pasien
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <!-- KOLOM 1 -->
                                        <form action="{{ route('simpan_daftar_pasien') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label>No registrasi</label>
                                                @foreach ($no_reg as $row)
                                                @endforeach
                                                <input type="text" class="form-control"
                                                    value="{{ $row->No_Registrasi + 1 }}" disabled>
                                            </div>

                                            <div class="form-group">
                                                <label>Nama Pasien</label>
                                                <input type="text" name="Nama_Pasien" id="Nama_Pasien"
                                                    class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Umur</label>
                                                <input type="text" name="Umur" id="Umur" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label class="d-block">Jenis Kelamin</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="Jenis_Kelamin"
                                                        value="Laki-laki">Laki-laki<br>
                                                    <input class="form-check-input" type="radio" name="Jenis_Kelamin"
                                                        value="Perempuan">Perempuan<br>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat Pasien</label>
                                                <input type="text" name="Alamat_Pasien" id="Alamat_Pasien"
                                                    class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>No Telepon Pasien</label>
                                            <input type="text" name="No_Telepon_Pasien" id="No_Telepon_Pasien"
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Rujukan</label>
                                            <input type="text" name="Rujukan" id="Rujukan" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Rujukan</label>
                                            <input type="text" name="Alamat_Rujukan" id="Alamat_Rujukan"
                                                class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Daftar</label>
                                            <input type="date" name="Tanggal_Daftar" id="Tanggal_Daftar"
                                                class="form-control">
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
