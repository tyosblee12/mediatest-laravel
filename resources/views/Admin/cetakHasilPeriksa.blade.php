<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>{{ $oke->havePasien[0]->Nama_Pasien }} - Hasil Periksa</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/starter-template/">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        .table-bordered {
            border: 1px solid black;
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <style type="text/css" media="print">
        @page {
            size: landscape;
            margin: 10px;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/css/starter-template.css') }}" rel="stylesheet">
</head>

<body>

    <div class="col-lg-10 mx-auto p-3 py-md-5">
        <header class="text-center pb-3 mb-3 border-bottom">
            <h2>LABORATORIUM KLINIK MEDIATEST SOREANG</h2>
            <p>Jl. Pesantren Bar. No.42, RT.002/RW.002, Pamekaran, Kec. Soreang, Kabupaten Bandung, Jawa Barat 40912</p>
        </header>

        <main>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card text-dark">
                        <div class="card-header">
                            Detail Hasil Pemeriksaan
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 border-right">
                                    {{-- KOLOM 1 --}}
                                    <div class="form-group">
                                        <label>Nomor ID Hasil</label>
                                        <h1> {{ $oke->id_hasil }}</h1>
                                    </div>
                                    <div class="form-group">
                                        <label>Nomor ID Pemeriksaan</label>
                                        <h1> {{ $oke->havePeriksa[0]->id_periksa }}</h1>
                                    </div>
                                </div>
                                <div class="col-md-5 pl-5 border-right">
                                    {{-- KOLOM 2 --}}
                                    <div class="form-group">
                                        <label>Nomor Registrasi Pasien</label>
                                        <h1> {{ $oke->havePasien[0]->No_Registrasi }}</h1>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Pasien</label>
                                        <h1> {{ $oke->havePasien[0]->Nama_Pasien }}</h1>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-5">
                                    {{-- KOLOM 3 --}}
                                    <div class="form-group m-0">
                                        <label>Laboratorium Kliknik Mediatest</label>
                                        <h5>{{ $oke->haveUser[0]->name }}</h5>
                                    </div>
                                    <div class="form-group m-0">
                                        <label>Tanggal Pemeriksaan</label>
                                        <h5> {{ $oke->tanggal_periksa }}</h5>
                                    </div>
                                    <div class="form-group m-0">
                                        <label>Tanggal Cetak</label>
                                        <h5> @php echo date("Y-m-d") @endphp</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="table table-responsive-sm">
                                <table class="table table-hover table-bordered mt-3">
                                    <thead>
                                        <tr>
                                            <th class="col-2">ID Jenis</th>
                                            <th class="col-5">Nama Jenis</th>
                                            <th class="col-3">Nilai</th>
                                            <th class="col-2">Harga</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($jenis as $item)
                                            <tr>
                                                <td> {{ $item->id_jenis }}</td>
                                                <td>{{ $item->nama_jenis }}</td>
                                                <td>0.25 > 0.55</td>
                                                <td>Rp. {{ $item->Harga }}</td>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan=3 class="text-center text-bold h6">TOTAL
                                            </td>
                                            <td class="text-left text-bold h6">
                                                Rp. {{ $total }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script>
        window.print();
    </script>

</body>

</html>
