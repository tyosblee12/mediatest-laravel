@extends('Admin.layout.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Selamat Datang, {{ session('name') }} </h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Periksa</a></div>
                    <div class="breadcrumb-item"><a href="#">Hasil</a></div>
                    <div class="breadcrumb-item">Detail</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card text-dark shadow-lg">
                            <div class="card-header bg-primary text-white">
                                Detail Hasil Pemeriksaan
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 border-right">
                                        {{-- KOLOM 1 --}}
                                        <div class="form-group">
                                            <label>Nomor ID Hasil</label>
                                            <h1> {{ $oke->id_hasil }}</h1>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor ID Pemeriksaan</label>
                                            <h1> {{ $oke->havePeriksa[0]->id_periksa }}</h1>
                                        </div>
                                    </div>
                                    <div class="col-md-5 pl-5 border-right">
                                        {{-- KOLOM 2 --}}
                                        <div class="form-group">
                                            <label>Nomor Registrasi Pasien</label>
                                            <h1> {{ $oke->havePasien[0]->No_Registrasi }}</h1>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pasien</label>
                                            <h1> {{ $oke->havePasien[0]->Nama_Pasien }}</h1>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-5">
                                        {{-- KOLOM 3 --}}
                                        <div class="form-group m-0">
                                            <label>Laboratorium Kliknik Mediatest</label>
                                            <h5>{{ $oke->haveUser[0]->name }}</h5>
                                        </div>
                                        <div class="form-group m-0">
                                            <label>Tanggal Pemeriksaan</label>
                                            <h5> {{ $oke->tanggal_periksa }}</h5>
                                        </div>
                                        <div class="form-group m-0">
                                            <label>Tanggal Cetak</label>
                                            <h5> {{ $oke->tanggal_periksa }}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="table table-responsive-sm">
                                    <table class="table table-hover table-bordered mt-3">
                                        <thead class="bg-primary">
                                            <tr>
                                                <th class="col-2 text-white">ID Jenis</th>
                                                <th class="col-6 text-white">Nama Jenis</th>
                                                <th class="col-2 text-white">Nilai</th>
                                                <th class="col-2 text-white">Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($jenis as $item)
                                                <tr>
                                                    <td> {{ $item->id_jenis }}</td>
                                                    <td>{{ $item->nama_jenis }}</td>
                                                    <td><input type="number" class="form-control text-right" id="id_user"
                                                            name="id_user" value="{{ session('id_user') }}">
                                                    </td>
                                                    <td>Rp. {{ $item->Harga }}</td>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot class="bg-primary">
                                            <tr>
                                                <td colspan=3 class="text-center text-white font-weight-bold">TOTAL</td>
                                                <td class="bg-grey text-left text-white font-weight-bold h5">
                                                    Rp. {{ $total }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <a href="{{ route('cetakHasilPeriksa', $oke->id_hasil) }}"
                                        class="btn btn-icon icon-left float-right btn-primary"><i class="fas fa-print"></i>
                                        Cetak</a>
                                    <a href="{{ route('tampilHasilPeriksa') }}"
                                        class="btn btn-icon icon-left float-left btn-danger" type="submit"><i
                                            class="fas fa-arrow-left"></i> Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
