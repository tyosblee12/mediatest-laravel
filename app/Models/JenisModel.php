<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisModel extends Model
{
    use HasFactory;

    protected $table    = "jenis_periksa";
    public $timestamps  = false;
    protected $fillable = ['id_jenis','nama','harga','is_active','created_at','updated_at'];

}