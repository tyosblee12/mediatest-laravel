<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilModel extends Model
{
    use HasFactory;
    protected $table = 'hasil';
    public $timestamps = false;
    protected $fillable = ['id_hasil', 'id_periksa', 'tanggal_periksa', 'id_user', 'is_active', 'created_at', 'No_Registrasi'];

    public function haveUser()
    {
        return $this->hasMany(User::class, 'id_user', 'id_user');
    }

    public function havePeriksa()
    {
        return $this->hasMany(PeriksaModel::class, 'id_periksa', 'id_periksa');
    }

    public function havePasien()
    {
        return $this->hasMany(PasienModel::class, 'No_Registrasi', 'No_Registrasi');
    }
}