<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PendaftaranModel extends Model
{
    protected $table    = "pasien";
    public $timestamps  = false;
    protected $fillable = ['No_Registrasi','Nama_Pasien', 'Umur', 'Jenis_Kelamin', 'Alamat_Pasien', 
    'No_Telepon_Pasien', 'Pengirim', 'Alamat_Pengirim', 'Tanggal_Daftar', 'Diagnosa','is_active'];

}