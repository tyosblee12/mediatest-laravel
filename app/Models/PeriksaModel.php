<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriksaModel extends Model
{
    use HasFactory;

    protected $table    = "periksa";
    public $timestamps  = false;
    protected $fillable = ['id_periksa','tanggal_periksa',
    'id_user','id_jenis','is_active','created_at', 'updated_at'];

    public function haveJenis()
    {
        return $this->hasMany(JenisModel::class, 'id_jenis', 
        'id_jenis');
    }
}