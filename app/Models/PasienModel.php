<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienModel extends Model
{
    use HasFactory;

    protected $table = 'Pasien';
    protected $fillable = ['No_Registrasi','Nama_Pasien','Umur','Jenis_Kelamin','Alamat_Pasien',
    'No_Telepon_Pasien','Rujukan','Alamat_Rujukan','Tanggal_Daftar','is_active' , 'created_at' , 'updated_at'];

    public function haveNo()
    {
        return $this->belongsTo(PemeriksaanModel::class, 'No_Registrasi', 'No_Registrasi');
    }

}