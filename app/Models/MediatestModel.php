<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediatestModel extends Model
{
    protected $table = "mediatesbaru";
    public $timestamps = false;
    protected $fillable = ['nama', 'email', 'phone', 'adress'];
}
