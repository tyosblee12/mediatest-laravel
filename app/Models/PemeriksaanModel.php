<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PemeriksaanModel extends Model
{
    use HasFactory;

    protected $table = 'pemeriksaan';
    public $timestamps = false;
    protected $fillable = ['Id_Periksa', 'Tanggal_Periksa', 'No_Registrasi', 'id_jenis', 'is_active', 'id_user'];

    public function havePasien()
    {
        return $this->hasMany(PasienModel::class, 'No_Registrasi', 'No_Registrasi');
    }

    public function haveJenis()
    {
        return $this->hasMany(JenisModel::class, 'id_jenis', 'id_jenis');
    }

    public function haveUser()
    {
        return $this->hasMany(PemeriksaanModel::class, 'id_user', 'id_user');
    }
}