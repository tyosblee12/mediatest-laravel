<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PemeriksaanModel;
use App\Models\JenisModel;
use App\Models\PasienModel;
use App\Models\PeriksaModel;
use App\Models\HasilModel;
use DB;

class PeriksaPasienController extends Controller
{
    // !TAMPIL
    public function tampilPeriksaPasien()
    {
        $jenis = JenisModel::all();
        $pasien = PasienModel::all();
        $periksa = PeriksaModel::where('is_active', 0)->get();
        $id_periksa = PeriksaModel::all();
        $baru = 1;
        $hasil = HasilModel::all();
        return view('Admin.data.PeriksaPasien', compact('jenis', 'pasien', 'periksa', 'id_periksa', 'baru', 'hasil'));
    }

    // !SIMPAN JENIS PERIKSA
    public function simpanJenisPeriksaPasien(Request $request, PeriksaModel $periksamodel)
    {
        $simpan = $periksamodel->create([
            'id_periksa' => $request->id_periksa,
            'tanggal_periksa' => now(),
            'id_jenis' => $request->id_jenis,
            'id_user' => $request->id_user,
            'created_at' => now(),
            'is_active' => 1,
        ]);
        if (!$simpan->exists) {
            return redirect()
                ->route('tampilPeriksaPasien')
                ->with('error', 'Data Gagal Disimpan');
        }
        $jenis = JenisModel::all();
        $pasien = PasienModel::all();
        $periksa = PeriksaModel::where('id_periksa', $request->id_periksa)->get();
        $id_periksa = PeriksaModel::all();
        $baru = 0;
        $hasil = HasilModel::all();
        notify()->success('Jenis Periksa berhasil di tambahkan');
        // return redirect()->route('tampilPeriksaPasien', compact('jenis','pasien','periksa', 'id_periksa'));
        return view('Admin.data.PeriksaPasien', compact('jenis', 'pasien', 'periksa', 'id_periksa', 'baru', 'hasil'))->with('success', 'Data Berhasil Disimpan');
    }

    // !SIMPAN JENIS PERIKSA
    public function simpanHasilPeriksa(Request $request, HasilModel $hasilmodel)
    {
        $simpan = $hasilmodel->create([
            'id_hasil' => $request->id_hasil,
            'id_periksa' => $request->id_periksa,
            'id_user' => $request->id_user,
            'No_Registrasi' => $request->No_Registrasi,
            'tanggal_periksa' => $request->tanggal_periksa,
            'created_at' => now(),
            'is_active' => 1,
        ]);
        if (!$simpan->exists) {
            return redirect()
                ->route('tampilPeriksaPasien')
                ->with('error', 'Data Gagal Disimpan');
        }
        notify()->success('Data', $request->id_hasil, 'berhasil disimpan ');
        return redirect()
            ->route('tampilHasilPeriksa')
            ->with('success', 'Data Berhasil Disimpan');
        // return view('Admin.data.PeriksaPasien', compact('jenis', 'pasien', 'periksa', 'id_periksa', 'baru'));
    }

    // !HAPUS JENIS PERIKSA

    public function hapusPeriksaPasien($id_jenis, $id_periksa)
    {
        $jenis = JenisModel::all();
        $pasien = PasienModel::all();
        $periksa = PeriksaModel::where('id_periksa', $id_periksa)->get();
        $id_periksa = PeriksaModel::all();
        $baru = 0;
        $hasil = HasilModel::all();

        $delete = DB::table('periksa')
            ->where('id_periksa', '=', $id_periksa, 'AND', 'id_jenis', '=', $id_jenis)
            ->delete();

        return view('Admin.data.PeriksaPasien', compact('jenis', 'pasien', 'periksa', 'id_periksa', 'baru', 'hasil'))->with('success', 'Data Berhasil Dihapus');
    }
}