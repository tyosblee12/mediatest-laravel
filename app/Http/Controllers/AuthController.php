<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\UserController;
use Hash;
use Auth;
use App\Models\User;
use Session;

class AuthController extends Controller
{
    public function getLogin()
    {
        return view('login.login');
    }

    // !UNTUK CEK LOGIN
    public function loginlogin(Request $request, User $user)
    {
        $email = $request->email;
        $pass = $request->password;
        $data = $user::where('email', $email)->first();

        // dd($data);
        if ($data) {
            if (Hash::check($pass, $data->password)) {
                session::put('name', $data->name);
                session::put('id_user', $data->id_user);
                session::put('email', $data->email);
                session::put('login', true);
                return redirect()->route('dashboard');
            } else {
                return redirect()
                    ->back()
                    ->with('error', 'Invalid Email address or Password');
            }
        } else {
            return redirect()
                ->back()
                ->with('error', 'No Data Found');
        }
    }

    // !LOGOUT ADMIN
    public function logout(Request $request)
    {
        if (\Auth::check()) {
            \Auth::logout();
            $request->session()->invalidate();
        }
        return view('login.login');
    }
}