<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PendaftaranModel;
use App\Models\PasienModel;

class DaftarController extends Controller
{
    public function index()
    {
        $no_reg = PasienModel::all();
        return view('Admin.data.daftarpasien', compact('no_reg'));
    }

    // !UNTUK SIMPEN DATA PENDAFATARAN PASIEN BARU, DARI FORM PENDAFTARAN
    public function simpan_daftar_pasien(Request $request, PasienModel $PasienModel)
    {
        $no_reg = PasienModel::all();
        foreach ($no_reg as $row) {
        }
        $id_reg = $row->No_Registrasi + 1;
        $simpan = $PasienModel->create([
            'No_Registrasi' => $id_reg,
            'Nama_Pasien' => $request->Nama_Pasien,
            'Umur' => $request->Umur,
            'Jenis_Kelamin' => $request->Jenis_Kelamin,
            'Alamat_Pasien' => $request->Alamat_Pasien,
            'No_Telepon_Pasien' => $request->No_Telepon_Pasien,
            'Rujukan' => $request->Rujukan,
            'Alamat_Rujukan' => $request->Alamat_Rujukan,
            'Tanggal_Daftar' => $request->Tanggal_Daftar,
            'is_active' => 1,
            'created_at' => now(),
        ]);

        if (!$simpan->exists) {
            return redirect()
                ->route('daftarpasien')
                ->with('error', 'Data Gagal Disimpan');
        }
        drakify('success');
        return redirect()
            ->route('data_pasien')
            ->with('success', 'Data Berhasil Disimpan');
    }
}