<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PendaftaranModel;
use App\Models\PemeriksaanModel;
use App\Models\PeriksaModel;
use App\Models\HasilModel;
use App\Models\User;
use DB;
use PDF;

class HasilController extends Controller
{
    public function tampilHasilPeriksa()
    {
        $hasil = HasilModel::where('is_active', 1)->get();
        return view('Admin.data.hasilperiksa', compact('hasil'));
    }

    public function tampilHasilDetail($id)
    {
        $oke = HasilModel::where('id_hasil', $id)->first();

        $jenis = DB::table('jenis_periksa')
            ->join('periksa', 'periksa.id_jenis', '=', 'jenis_periksa.id_jenis')
            ->join('hasil', 'hasil.id_periksa', '=', 'periksa.id_periksa')
            ->where('hasil.id_hasil', $id)
            ->get();

        $total = DB::table('jenis_periksa')
            ->select(DB::raw('SUM(jenis_periksa.Harga) as total'))
            ->join('periksa', 'periksa.id_jenis', '=', 'jenis_periksa.id_jenis')
            ->join('hasil', 'hasil.id_periksa', '=', 'periksa.id_periksa')
            ->where('hasil.id_hasil', $id)
            ->get()
            ->first()->total;
        // dd($oke);
        return view('Admin.data.hasildetail', compact('oke', 'jenis', 'total'));
    }

    public function cetakHasilPeriksa($id)
    {
        $oke = HasilModel::where('id_hasil', $id)->first();

        $jenis = DB::table('jenis_periksa')
            ->join('periksa', 'periksa.id_jenis', '=', 'jenis_periksa.id_jenis')
            ->join('hasil', 'hasil.id_periksa', '=', 'periksa.id_periksa')
            ->where('hasil.id_hasil', $id)
            ->get();

        $total = DB::table('jenis_periksa')
            ->select(DB::raw('SUM(jenis_periksa.Harga) as total'))
            ->join('periksa', 'periksa.id_jenis', '=', 'jenis_periksa.id_jenis')
            ->join('hasil', 'hasil.id_periksa', '=', 'periksa.id_periksa')
            ->where('hasil.id_hasil', $id)
            ->get()
            ->first()->total;

        return view('Admin.cetakHasilPeriksa', compact('oke', 'jenis', 'total'));
        // $pdf = PDF::loadview('Admin.cetakHasilPeriksa', ['oke' => $oke, 'jenis' => $jenis, 'total' => $total]);
        // return $pdf->download($oke, 'hasil-periksa-pdf');
    }

    // !HAPUS DATA HASIL
    public function hapusHasilPeriksa($id)
    {
        $hapus = HasilModel::where('id_hasil', $id)->update([
            'is_active' => 0,
        ]);
        notify()->success('Data Berhasil di Hapus');
        return redirect()->route('tampilHasilPeriksa');
    }

    // !ISI HASIL
    public function inputHasilAwal($id)
    {
        $jenis = PeriksaModel::find('id_jenis', $id);
        dd($jenis);
        return response()->json([
            'data' => $jenis,
        ]);
    }

    public function update($id)
    {
        $category = HasilModel::find($id);

        return response()->json([
            'data' => $category,
        ]);
    }

    // !=============================================================================== //

    public function index($id)
    {
        $pasien = PendaftaranModel::where('no_reg', $id)->all();
        $pemeriksaan = PemeriksaanModel::where('no_reg', $id)->all();
        $hasil = HasilModel::where('no_reg', $id)->all();
        return view('Admin.cetak.hasil.semuanya', compact('pasien', 'pemeriksaan', 'hasil'));
    }

    public function tampilHasil()
    {
        $pasien = PendaftaranModel::all();
        $pemeriksaan = PemeriksaanModel::all();
        return view('Admin.data.cetakhasil', compact('pasien', 'pemeriksaan'));
    }
}