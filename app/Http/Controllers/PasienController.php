<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PendaftaranModel;
use App\Models\PemeriksaanModel;
use App\Models\JenisModel;
use App\Models\PasienModel;

class PasienController extends Controller
{
    //UNTUK MENAMPILKAN DATA PASIEN
    public function datapasien()
    {
        $datapasien = PendaftaranModel::where('is_active', 1)->get();
        $no_reg = PasienModel::all();
        return view('Admin.data.pasien', compact('datapasien', 'no_reg'));
    }

    #EDIT DATA
    public function editDataPasien($id)
    {
        $pasien = PendaftaranModel::where('no_registrasi', $id)->first();
        return view('Admin.data.editdatapasien', compact('pasien'));
    }

    #UPDATE DATA
    public function updateDataPasien($id, Request $request, PendaftaranModel $PendaftaranModel)
    {
        $simpan = $PendaftaranModel->where('No_Registrasi', $id)->update([
            'Nama_Pasien' => $request->Nama_Pasien,
            'Umur' => $request->Umur,
            'Jenis_Kelamin' => $request->Jenis_Kelamin,
            'Alamat_Pasien' => $request->Alamat_Pasien,
            'Rujukan' => $request->Rujukan,
            'Alamat_Rujukan' => $request->Alamat_Rujukan,
            'No_Telepon_Pasien' => $request->No_Telepon_Pasien,
            'Tanggal_Daftar' => $request->Tanggal_Daftar,
            'updated_at' => now(),
        ]);

        if (!$simpan) {
            return redirect()
                ->route('data_pasien')
                ->with('error', 'Data Gagal di Update');
        }

        return redirect()
            ->route('data_pasien')
            ->with('success', 'Data Berhasil di Update');
    }

    // HARD DELETE
    public function deleteDataPasien($id)
    {
        $pasien = PendaftaranModel::where('No_Registrasi', $id)->delete();
        return redirect()->route('data_pasien');
    }

    // SOFT DELETE
    public function deletePasien($id)
    {
        $pasien = PendaftaranModel::where('No_Registrasi', $id)->update([
            'is_active' => 0,
        ]);
        return redirect()->route('data_pasien');
    }

    // PEMERIKSAAN PASIEN YANG TERDAFTAR
    public function periksaData($id)
    {
        $pasien = PendaftaranModel::where('no_registrasi', $id)->first();
        $pemeriksaan = PemeriksaanModel::all();
        $jenis = JenisModel::all();
        foreach ($pemeriksaan as $row) {
        }
        $id_periksa = $row->Id_Periksa + 1;
        return view('Admin.data.dataperiksa', compact('pasien', 'id_periksa', 'jenis'));
    }

    // SIMPAN DATA PEMERIKSAAN
    public function simpanperiksaData(Request $request, PemeriksaanModel $pemeriksaanModel)
    {
        $pemeriksaan = PemeriksaanModel::all();
        foreach ($pemeriksaan as $row) {
        }

        $simpan = $pemeriksaanModel->create([
            'Id_Periksa' => $id_periksa,
            'Tanggal_Periksa' => $request->Tanggal_Periksa,
            'No_Registrasi' => $request->No_Registrasi,
            'id_jenis' => $request->id_jenis,
            'id_user' => 17,
            'is_active' => 1,
            'created_at' => now(),
        ]);

        if (!$simpan->exists) {
            return redirect()
                ->route('data_pasien')
                ->with('error', 'Data Gagal Disimpan');
        }
        return redirect()
            ->route('periksa_data')
            ->with('success', 'Data Berhasil Disimpan');
    }
}