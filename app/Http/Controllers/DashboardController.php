<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PendaftaranModel;

class DashboardController extends Controller
{
    public function index()
    {
        $datapasien = PendaftaranModel::where('is_active', 1)->get();

        $pasien = PendaftaranModel::where('is_active', 1)->count();

        return view('Admin.dashboard', compact('datapasien', 'pasien'));
    }
}