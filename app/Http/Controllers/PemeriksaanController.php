<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PemeriksaanModel;
use App\Models\PasienModel;
use App\Models\JenisModel;
use Carbon\Carbon;
use DB;
use PDF;

class PemeriksaanController extends Controller
{
    //UNTUK MENAMPILKAN DATA PEMERIKSAAN
    public function tampilDataPemeriksaan()
    {
        $datapemeriksaan = PemeriksaanModel::where('is_active', 1)->get();
        // dd($datapemeriksaan);
        return view('Admin.data.pemeriksaan', compact('datapemeriksaan'));
    }

    //EDIT DATA PEMERIKSAAN
    public function editDataPemeriksaan($id)
    {
        $jenis = JenisModel::all();
        $periksa = PemeriksaanModel::where('Id_Periksa', $id)->first();
        return view('Admin.data.editpemeriksaan', compact('periksa', 'jenis'));
    }

    #UPDATE DATA PEMERIKSAAN
    public function updateDataPemeriksaan($id, Request $request, PemeriksaanModel $PemeriksaanModel)
    {
        $update = $PemeriksaanModel->where('Id_Periksa', $id)->update([
            'id_jenis' => $request->id_jenis,
            'updated_at' => now(),
        ]);

        if (!$update) {
            return redirect()
                ->route('tampilDataPemeriksaan')
                ->with('error', 'Data Gagal di Update');
        }

        return redirect()
            ->route('tampilDataPemeriksaan')
            ->with('success', 'Data Berhasil di Update');
    }

    // SOFT DELETE
    public function deleteDataPemeriksaan($id)
    {
        $pasien = PemeriksaanModel::where('Id_Periksa', $id)->update([
            'is_active' => 0,
        ]);
        return redirect()->route('tampilDataPemeriksaan');
    }

    // ?HASIL DATA PEMERIKSAAN

    public function hasilDataPemeriksaan($id, Request $request, PemeriksaanModel $PemeriksaanModel)
    {
        $Id_Periksa = $request->Id_Periksa;
        $No_Registrasi = $request->No_Registrasi;
        $date = $request->Tanggal_Periksa;
        // dd($date);
        $hasil = PemeriksaanModel::where('Id_Periksa', $id)->first();
        $jenis = PemeriksaanModel::all()
            ->where('Tanggal_Periksa', $date)
            ->where('No_Registrasi', $No_Registrasi);

        $total = DB::table('jenis_periksa')
            ->select(DB::raw('SUM(jenis_periksa.Harga) as total'))
            ->join('pemeriksaan', 'pemeriksaan.id_jenis', '=', 'jenis_periksa.id_jenis')
            ->where('pemeriksaan.Tanggal_Periksa', $date)
            ->where('pemeriksaan.No_Registrasi', $No_Registrasi)
            ->get()
            ->first()->total;

        return view('Admin.data.hasilpemeriksaan', compact('hasil', 'jenis', 'total'));
    }

    // ?CETAK HASIL
    public function cetak_pdf()
    {
        $jenis = PemeriksaanModel::all();
        $pdf = PDF::loadview('Admin.data.cetakhasil', ['Pemeriksaan' => $jenis]);
        return $pdf->download('laporan-pemeriksaan-pdf');
    }
}
