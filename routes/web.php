<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DaftarController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PasienController;
use App\Http\Controllers\PemeriksaanController;
use App\Http\Controllers\HasilController;
use App\Http\Controllers\PeriksaPasienController;

#INI ROUTE LOGIN
Route::get('/', [AuthController::class, 'getLogin']);
Route::post('/signin', [AuthController::class, 'loginlogin'])->name('ceklogin');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

#INI ROUTE DASHBOARD
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

// DAFTAR PASIEN
Route::get('dashboard/daftarpasien/', [DaftarController::class, 'index'])->name('daftarpasien');
Route::post('dashboard/daftarpasien/simpan', [DaftarController::class, 'simpan_daftar_pasien'])->name('simpan_daftar_pasien');

// DATA PASIEN DAFTAR
Route::get('/dashboard/datapasien', [PasienController::class, 'datapasien'])->name('data_pasien');
Route::get('/dashboard/pasien/edit/{id}', [PasienController::class, 'editDataPasien'])->name('edit_data');
Route::post('dashboard/pasien/update/{id}', [PasienController::class, 'updateDataPasien'])->name('update_data');
Route::get('/dashboard/pasien/delete/{id}', [PasienController::class, 'deletePasien'])->name('delete_data');

// DARI PASIEN YANG DI PERIKSA (DIAGNOSA)
Route::get('/dashboard/dataperiksa/{id}', [PasienController::class, 'periksaData'])->name('periksa_data');
Route::post('dashboard/dataperiksa/simpan', [PasienController::class, 'simpanperiksaData'])->name('simpan_data_periksa');

// DATA PEMERIKSAAAN
Route::get('/dashboard/Pemeriksaan', [PemeriksaanController::class, 'tampilDataPemeriksaan'])->name('tampilDataPemeriksaan');
Route::get('/dashboard/Pemeriksaan/edit/{id}', [PemeriksaanController::class, 'editDataPemeriksaan'])->name('editDataPemeriksaan');
Route::post('/dashboard/Pemeriksaan/update/{id}', [PemeriksaanController::class, 'updateDataPemeriksaan'])->name('updateDataPemeriksaan');
Route::get('/dashboard/Pemeriksaan/delete/{id}', [PemeriksaanController::class, 'deleteDataPemeriksaan'])->name('deleteDataPemeriksaan');
Route::post('/dashboard/Pemeriksaan/hasil/{id}', [PemeriksaanController::class, 'hasilDataPemeriksaan'])->name('hasilDataPemeriksaan');
Route::post('dashboard/Pemeriksaan/simpan', [PemeriksaanController::class, 'simpanHasilPemeriksaan'])->name('simpanHasilPemeriksaan');

// HASIL
Route::get('/dashboard/Hasil/', [HasilController::class, 'tampilHasil'])->name('tampilHasil');

// !BARU
Route::get('/dashboard/Periksa', [PeriksaPasienController::class, 'tampilPeriksaPasien'])->name('tampilPeriksaPasien');
Route::Post('/dashboard/Periksa', [PeriksaPasienController::class, 'simpanJenisPeriksaPasien'])->name('simpanJenisPeriksaPasien');
Route::get('/dashboard/Periksa/Hapus/{id_periksa}/{id_jenis}', [PeriksaPasienController::class, 'hapusPeriksaPasien'])->name('hapusPeriksaPasien');

Route::Post('/dashboard/hasil', [PeriksaPasienController::class, 'simpanHasilPeriksa'])->name('simpanHasilPeriksa');
Route::get('/dashboard/hasil', [HasilController::class, 'tampilHasilPeriksa'])->name('tampilHasilPeriksa');
Route::get('/dashboard/hasil/{id}', [HasilController::class, 'tampilHasilDetail'])->name('tampilHasilDetail');
Route::get('/dashboard/hasil/hapus/{id}', [HasilController::class, 'hapusHasilPeriksa'])->name('hapusHasilPeriksa');

Route::get('/color/{id}/edit', 'HasilController@update')->name('color.update');
// Route::get('/dashboard/input/hasil/{id}', [HasilController::class, 'inputHasilAwal'])->name('inputHasilAwal');

// !CETAK PDF
Route::get('/dashboard/cetak/{id}', [HasilController::class, 'cetakHasilPeriksa'])->name('cetakHasilPeriksa');