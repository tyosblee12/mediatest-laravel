<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemeriksaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan', function (Blueprint $table) {
            $table->integer('Id_Periksa');
            $table->date('Tanggal_Periksa');
            $table->integer('No_Registrasi');
            $table->foreign('No_Registrasi')->references('No_Registrasi')->on('Pasien');
            $table->integer('id_jenis');
            $table->foreign('id_jenis')->references('id_jenis')->on('Jenis_Periksa');
            $table->integer('id_user');
            $table->foreign('id_user')->references('id_user')->on('users');
            $table->integer('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaan');
    }
}