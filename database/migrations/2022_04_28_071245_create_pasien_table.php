'<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien', function (Blueprint $table) {
            $table->integer('No_Registrasi')->primary();
            $table->string('Nama_Pasien');
            $table->integer('Umur');
            $table->string('Jenis_Kelamin');
            $table->text('Alamat_Pasien');
            $table->integer('No_Telepon_Pasien');
            $table->string('Rujukan');
            $table->text('Alamat_Rujukan');
            $table->date('Tanggal_Daftar');
            $table->integer('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
