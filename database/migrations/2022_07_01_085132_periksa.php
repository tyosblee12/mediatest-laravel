<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Periksa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periksa', function (Blueprint $table) {
            $table->id();
            $table->integer('id_periksa');
            $table->date('tanggal_periksa');
            $table->integer('id_jenis');
            $table
                ->foreign('id_jenis')
                ->references('id_jenis')
                ->on('Jenis_Periksa');
            $table->integer('id_user');
            $table
                ->foreign('id_user')
                ->references('id_user')
                ->on('users');
            $table->tinyinteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periksa');
    }
}