<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Hasil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil', function (Blueprint $table) {
            $table->integer('id_hasil')->primary();
            $table->foreignId('id_periksa')->on('periksa');
            // $table
            //     ->foreignId('id_periksa')
            //     ->references('id_periksa')
            //     ->on('periksa');
            $table->integer('No_Registrasi');
            $table
                ->foreign('No_Registrasi')
                ->references('No_Registrasi')
                ->on('pasien');
            $table->date('tanggal_periksa');
            $table->integer('id_user');
            $table
                ->foreign('id_user')
                ->references('id_user')
                ->on('users');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil');
    }
}