<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Nilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai', function (Blueprint $table) {
            $table->integer('id_nilai')->primary();
            $table->integer('id_hasil');
            $table
                ->foreign('id_hasil')
                ->references('id_hasil')
                ->on('hasil');
            $table->integer('No_Registrasi');
            $table
                ->foreign('No_Registrasi')
                ->references('No_Registrasi')
                ->on('pasien');
            $table->integer('id_user');
            $table
                ->foreign('id_user')
                ->references('id_user')
                ->on('users');
            $table->integer('id_jenis');
            $table
                ->foreign('id_jenis')
                ->references('id_jenis')
                ->on('jenis_periksa');
            $table->decimal('nilai');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai');
    }
}