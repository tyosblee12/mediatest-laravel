<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class JenisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_periksa')->insert([
            [
                'id_jenis'          => '1',
                'nama_jenis'        => 'Paket Darah Rutin' ,
                'Harga'             =>  100000,
                'is_active'         =>  1,
                'created_at'        => now(),
                
            ],
            [
                'id_jenis'          => '2',
                'nama_jenis'        => 'Paket Darah Lengkap' ,
                'Harga'             =>  130000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ], 
            [
                'id_jenis'          => '3',
                'nama_jenis'        => 'Trombosit' ,
                'Harga'             =>  30000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],  
            [
                'id_jenis'          => '4',
                'nama_jenis'        => 'Lekosit' ,
                'Harga'             =>  30000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],  
            [
                'id_jenis'          => '5',
                'nama_jenis'        => 'Waktu Pendarahan(BT)' ,
                'Harga'             =>  25000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '6',
                'nama_jenis'        => 'Kolestrol Total' ,
                'Harga'             =>  35000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '7',
                'nama_jenis'        => 'Asam Urat' ,
                'Harga'             =>  35000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '8',
                'nama_jenis'        => 'Kreatinim' ,
                'Harga'             =>  50000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '9',
                'nama_jenis'        => 'HB, PROTEIN URIN, HbsAg, HIV, Siphilis' ,
                'Harga'             =>  400000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '10',
                'nama_jenis'        => 'ASTO' ,
                'Harga'             =>  90000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '11',
                'nama_jenis'        => 'Anti HBs' ,
                'Harga'             =>  250000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '12',
                'nama_jenis'        => 'Sifilis' ,
                'Harga'             =>  90000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '13',
                'nama_jenis'        => 'Urin Rutin' ,
                'Harga'             =>  50000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '14',
                'nama_jenis'        => 'Glukosa Urin' ,
                'Harga'             =>  35000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '15',
                'nama_jenis'        => 'Test Narkoba' ,
                'Harga'             =>  400000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '16',
                'nama_jenis'        => 'Faeces Rutin' ,
                'Harga'             =>  70000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '17',
                'nama_jenis'        => 'Preparat Langsung' ,
                'Harga'             =>  50000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '18',
                'nama_jenis'        => 'Pewarnaan BTA 1x' ,
                'Harga'             =>  50000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '19',
                'nama_jenis'        => 'Pewarnaan BTA 3x' ,
                'Harga'             =>  150000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '20',
                'nama_jenis'        => 'Thorax Dewasa' ,
                'Harga'             =>  150000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '21',
                'nama_jenis'        => 'Thorax Anak' ,
                'Harga'             =>  150000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '22',
                'nama_jenis'        => 'Genu' ,
                'Harga'             =>  300000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '23',
                'nama_jenis'        => 'Shoulder' ,
                'Harga'             =>  300000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '24',
                'nama_jenis'        => 'Humerus' ,
                'Harga'             =>  300000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            [
                'id_jenis'          => '25',
                'nama_jenis'        => 'Na,K' ,
                'Harga'             =>  350000,
                'is_active'         =>  1,
                'created_at'        => now(),
            ],
            ]);
    
    }
}