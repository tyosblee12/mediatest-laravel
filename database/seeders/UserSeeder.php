<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'id_user' => 021,
                'name' => 'Indah Pratiwi',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('admin'),
                'created_at' => NOW(),
            ),
        ));
    }
}