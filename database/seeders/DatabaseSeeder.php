<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(PasienSeeder::class);
        $this->call(JenisSeeder::class);
        $this->call(PemeriksaanSeeder::class);
        $this->call(PeriksaSeeder::class);
        $this->call(HasilSeeder::class);
        $this->call(NilaiSeeder::class);
    }
}