<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data ke table pasien
        DB::table('pasien')->insert([
            [
                'No_Registrasi'     => '9220342',
                'Nama_Pasien'       => 'Dudi Darmawan',
                'Umur'              => '23',
                'Jenis_Kelamin'     => 'Laki-Laki',
                'Alamat_Pasien'     => 'Kiaracondong',
                'Rujukan'          => 'Rumah Sakit Bandung',
                'Alamat_Rujukan'   => 'Bandung',
                'No_Telepon_Pasien' => '082315725',
                'Tanggal_Daftar'    => '2022-04-18',
                'is_active'         =>  1,
                'created_at'        => now(),
                
            ],
            [  
                'No_Registrasi'     => '9220343',
                'Nama_Pasien'       => 'Vani Fabiola',
                'Umur'              => '24',
                'Jenis_Kelamin'     => 'Perempuan',
                'Alamat_Pasien'     => 'Cisewu',
                'Rujukan'          => 'Rumah Sakit Garut',
                'Alamat_Rujukan'   => 'Bandung',
                'No_Telepon_Pasien' => '082314627',
                'Tanggal_Daftar'    => '2022-04-18',
                'is_active'         =>  1,
                'created_at'        => now(),

            ],
            [
                'No_Registrasi'     => '9220344',
                'Nama_Pasien'       => 'Asep Rahmat',
                'Umur'              => '25',
                'Jenis_Kelamin'     => 'Laki-Laki',
                'Alamat_Pasien'     => 'Cimahi',
                'Rujukan'          => 'Rumah Sakit Dustira',
                'Alamat_Rujukan'   => 'Jalan Dustira',
                'No_Telepon_Pasien' => '0866729239',
                'Tanggal_Daftar'    => '2022-05-19',
                'is_active'         =>  1,
                'created_at'        => now(),
                
            ],
        ]);
    }
}