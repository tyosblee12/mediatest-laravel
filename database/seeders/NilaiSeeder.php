<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class NilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nilai')->insert([
            [
                'id_nilai' => 10,
                'id_hasil' => 131,
                'no_registrasi' => 9220342,
                'id_jenis' => 1,
                'nilai' => '0.25',
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
            [
                'id_nilai' => 11,
                'id_hasil' => 131,
                'no_registrasi' => 9220342,
                'id_jenis' => 2,
                'nilai' => '1.25',
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
        ]);
    }
}