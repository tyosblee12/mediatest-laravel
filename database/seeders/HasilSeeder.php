<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class HasilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hasil')->insert([
            [
                'id_hasil' => 131,
                'id_periksa' => 313054,
                'No_Registrasi' => 9220342,
                'tanggal_periksa' => now(),
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
            [
                'id_hasil' => 132,
                'id_periksa' => 313055,
                'No_Registrasi' => 9220343,
                'tanggal_periksa' => now(),
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
        ]);
    }
}