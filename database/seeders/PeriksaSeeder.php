<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PeriksaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periksa')->insert([
            [
                'id_periksa' => '313054',
                'tanggal_periksa' => now(),
                'id_jenis' => 1,
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
            [
                'id_periksa' => '313054',
                'tanggal_periksa' => now(),
                'id_jenis' => 2,
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
            [
                'id_periksa' => '313054',
                'tanggal_periksa' => now(),
                'id_jenis' => 4,
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
            [
                'id_periksa' => '313055',
                'tanggal_periksa' => now(),
                'id_jenis' => 7,
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
            [
                'id_periksa' => '313055',
                'tanggal_periksa' => now(),
                'id_jenis' => 5,
                'id_user' => 17,
                'is_active' => 1,
                'created_at' => now(),
            ],
        ]);
    }
}