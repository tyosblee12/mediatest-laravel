<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PemeriksaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data ke table pasien
        DB::table('pemeriksaan')->insert([
            [
                'Id_Periksa'            => '313054',
                'Tanggal_Periksa'       => now(),
                'No_Registrasi'         => '9220344',
                'id_jenis'              => 1,
                'id_user'               => 021,
                'is_active'             =>  1,
                'created_at'            => now(),
                
            ],
            [  
                'Id_Periksa'            => '313054',
                'Tanggal_Periksa'       => now(),
                'No_Registrasi'         => '9220344',
                'id_jenis'              => 2,
                'id_user'               => 021,
                'is_active'             =>  1,
                'created_at'            => now(),

            ],[
                'Id_Periksa'            => '313055',
                'Tanggal_Periksa'       => now(),
                'No_Registrasi'         => '9220343',
                'id_jenis'              => 1,
                'id_user'               => 021,
                'is_active'             =>  1,
                'created_at'            => now(),
                
            ]
        ]);
    }
}